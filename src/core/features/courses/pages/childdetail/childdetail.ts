import { Component, OnDestroy } from '@angular/core';
import { CoreDomUtils } from '@services/utils/dom';
import { CoreCourses } from '@features/courses/services/courses';
import { CoreSites } from '@services/sites';
import { CoreNavigator } from '@services/navigator';
import { CoreCourseHelper } from '@features/course/services/course-helper';
import { child } from '@features/courses/services/child-courses';
import { CoreCourseBlock } from '@features/course/services/course';
import { CoreBlockDelegate } from '@features/block/services/block-delegate';
import { CoreCoursesDashboard } from '@features/courses/services/dashboard';

/**
 * Generated class for the ChilddetailPage page.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

// @IonicPage({ segment: "core-child-detail" })
@Component({
  selector: 'childdetail',
  templateUrl: 'childdetail.html'
})

export class ChilddetailPage implements OnDestroy {

  child: child
  downloadCourseEnabled?: boolean;
  updateSiteObserver;
  courses: any[] = [];
  block: any = {
    name: 'myoverview',
    visible: true,
  };
  hasMainBlocks = false;
  loaded = false;

  constructor() {
    this.child = CoreNavigator.getRouteParam('child') as child;
  }

  /**
     * Load fallback blocks to shown before 3.6 when dashboard blocks are not supported.
     */
  protected loadFallbackBlocks(): void {
    this.block = {
      name: 'myoverview',
      visible: true,
    };

    this.hasMainBlocks = CoreBlockDelegate.isBlockSupported('myoverview');
}

  /**
     * Convenience function to fetch the dashboard data.
     *
     * @returns Promise resolved when done.
     */
  protected async loadContent(): Promise<void> {
    const available = await CoreCoursesDashboard.isAvailable();
    const disabled = await CoreCoursesDashboard.isDisabled();

    if (available && !disabled) {
        try {
            const blocks = await CoreCoursesDashboard.getDashboardBlocks();

            this.block = blocks.mainBlocks?.filter((block: CoreCourseBlock) => block.name === "myoverview")[0];

            this.hasMainBlocks = CoreBlockDelegate.hasSupportedBlock(blocks.mainBlocks);

        } catch (error) {
            CoreDomUtils.showErrorModal(error);

            // Cannot get the blocks, just show dashboard if needed.
            this.loadFallbackBlocks();
        }
    } else if (!available) {
        // Not available, but not disabled either. Use fallback.
        this.loadFallbackBlocks();
    } else {
        // Disabled.
        this.block = {
          name: 'myoverview',
          visible: true,
        };
    }

    this.loaded = true;

    // this.logView();
  }

  ngOnInit(): void {
   this.loadContent();
  //  this.get_student_courses();
}


get_student_courses(){
    return CoreCourses.getCoursesByField('ids', this.child.child_courses_ids).then((courses) => {
      const currentSite = CoreSites.getCurrentSite();
      for(var child_course_key in courses){
        courses[child_course_key]['courseimage'] = courses[child_course_key]['overviewfiles'][0]['fileurl']+"?token="+currentSite?.getToken();
      }

      this.courses = courses;
    }).catch((error) => {
      CoreDomUtils.showErrorModalDefault(error, 'core.courses.errorloadcourses', true);
    });
}




  /**
     * Open a course.
     *
     * @param course The course to open.
     */
   openCourse(course: any): void {
   var pageParams: any = {
      sectionId: 10
        };
        CoreCourseHelper.openCourse(course, pageParams);

}

goBack(): void {
  CoreNavigator.back();
}

    /**
   * Component being destroyed.
   */
     ngOnDestroy(): void {
      this.updateSiteObserver && this.updateSiteObserver.off();
    }

}
