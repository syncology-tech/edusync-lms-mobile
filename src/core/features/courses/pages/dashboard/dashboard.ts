// (C) Copyright 2015 Moodle Pty Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';

import { CoreCourses } from '../../services/courses';
import { CoreEventObserver, CoreEvents } from '@singletons/events';
import { CoreSites } from '@services/sites';
import { CoreCoursesDashboard } from '@features/courses/services/dashboard';
import { CoreDomUtils } from '@services/utils/dom';
import { CoreCourseBlock } from '@features/course/services/course';
import { CoreBlockComponent } from '@features/block/components/block/block';
import { CoreNavigator } from '@services/navigator';
import { CoreBlockDelegate } from '@features/block/services/block-delegate';
import { CoreTime } from '@singletons/time';
import { CoreAnalytics, CoreAnalyticsEventType } from '@services/analytics';
import { Translate } from '@singletons';
import { CoreUtils } from '@services/utils/utils';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { child } from '@features/courses/services/child-courses';

/**
 * Page that displays the dashboard page.
 */
@Component({
    selector: 'page-core-courses-dashboard',
    templateUrl: 'dashboard.html',
})
export class CoreCoursesDashboardPage implements OnInit, OnDestroy {

    @ViewChildren(CoreBlockComponent) blocksComponents?: QueryList<CoreBlockComponent>;

    hasMainBlocks = false;
    hasSideBlocks = false;
    searchEnabled = false;
    downloadCourseEnabled = false;
    downloadCoursesEnabled = false;
    userId?: number;
    blocks: Partial<CoreCourseBlock>[] = [];
    loaded = false;

    protected updateSiteObserver: CoreEventObserver;
    protected logView: () => void;

    constructor(private http: HttpClient) {
        // Refresh the enabled flags if site is updated.
        this.updateSiteObserver = CoreEvents.on(CoreEvents.SITE_UPDATED, () => {
            this.searchEnabled = !CoreCourses.isSearchCoursesDisabledInSite();
            this.downloadCourseEnabled = !CoreCourses.isDownloadCourseDisabledInSite();
            this.downloadCoursesEnabled = !CoreCourses.isDownloadCoursesDisabledInSite();

        }, CoreSites.getCurrentSiteId());

        this.logView = CoreTime.once(async () => {
            await CoreUtils.ignoreErrors(CoreCourses.logView('dashboard'));

            CoreAnalytics.logEvent({
                type: CoreAnalyticsEventType.VIEW_ITEM,
                ws: 'core_my_view_page',
                name: Translate.instant('core.courses.mymoodle'),
                data: { category: 'course', page: 'dashboard' },
                url: '/my/',
            });
        });
    }

    /**
     * @inheritdoc
     */
    ngOnInit(): void {
        this.searchEnabled = !CoreCourses.isSearchCoursesDisabledInSite();
        this.downloadCourseEnabled = !CoreCourses.isDownloadCourseDisabledInSite();
        this.downloadCoursesEnabled = !CoreCourses.isDownloadCoursesDisabledInSite();

        this.loadContent();
    }

    check_image_URL(url: string) : string{
        var image_url = "";
        if (url && url.trim() !== ''){
          image_url = url;
        }
        else{
          image_url = "https://ionicframework.com/docs/demos/api/avatar/avatar.svg";
        }

        return image_url;
    }

    /**
     * Refresh the dashboard data.
     *
     * @param refresher Refresher.
     */
    async get_children() {
        this.get_children_data().subscribe(data => {
            if(data) {
                const children: child[] = [];
                const json = JSON.parse(data);
                for(var child_key in json){
                    if(json.hasOwnProperty(child_key)){
                        var child_obj: child = {
                            child_email: json[child_key]['child_email'],
                            child_id: json[child_key]['child_id'],
                            child_image_url:  this.check_image_URL(json[child_key]['child_image_url']),
                            child_name: json[child_key]['child_name'],
                            child_courses_ids: "",
                            child_courses: [],
                            child_report_link:  json[child_key]['child_reportlink']
                        };
                        let child_courses_ids: any[] = [];
                        for(var child_course_key in json[child_key]['child_courses']){
                            child_obj.child_courses?.push(
                                {
                                    id: json[child_key]['child_courses'][child_course_key]['id'],
                                    course_image_url: "",
                                    name:json[child_key]['child_courses'][child_course_key]['name'],
                                }
                            )
                            child_courses_ids.push(json[child_key]['child_courses'][child_course_key]['id'])
                        }

                        child_obj.child_courses_ids = child_courses_ids.toString();
                        child_obj.on_child_click = (child: child) => {
                            CoreNavigator.navigate('childdetail', {
                                params: { child },
                            } as any);
                        }
                        children.push(child_obj);
                    }
                }
                this.blocks = this.blocks.filter((block: CoreCourseBlock) => block.name !== 'parents' && block.name !== 'myoverview');
                // this.blocks.unshift({ name: 'mychildren', visible: true, localBlock: true, contents: { localContents: { children } } });
            }
        })
    }

    get_children_data(): Observable<any> {
        let userId = this.userId ? this.userId : CoreSites.getCurrentSiteUserId();
        var current_site = CoreSites.getCurrentSite();
        let current_site_url = current_site?.siteUrl;
        let url = `${current_site_url}/webservice/rest/server.php?wstoken=6cfa7f60bf579ba0d59b779bad638364&wsfunction=get_child&moodlewsrestformat=json&parentid=${userId}`
        var response = this.http.get(url);
        return response
    }

    /**
     * Convenience function to fetch the dashboard data.
     *
     * @returns Promise resolved when done.
     */
    protected async loadContent(): Promise<void> {
        const available = await CoreCoursesDashboard.isAvailable();
        const disabled = await CoreCoursesDashboard.isDisabled();

        if (available && !disabled) {
            this.userId = CoreSites.getCurrentSiteUserId();

            try {
                const blocks = await CoreCoursesDashboard.getDashboardBlocks();

                this.blocks = blocks.mainBlocks;

                this.hasMainBlocks = CoreBlockDelegate.hasSupportedBlock(blocks.mainBlocks);
                this.hasSideBlocks = CoreBlockDelegate.hasSupportedBlock(blocks.sideBlocks);

                this.get_children();
            } catch (error) {
                CoreDomUtils.showErrorModal(error);

                // Cannot get the blocks, just show dashboard if needed.
                this.loadFallbackBlocks();
            }
        } else if (!available) {
            // Not available, but not disabled either. Use fallback.
            this.loadFallbackBlocks();
        } else {
            // Disabled.
            this.blocks = [];
        }

        this.loaded = true;

        this.logView();
    }

    /**
     * Load fallback blocks to shown before 3.6 when dashboard blocks are not supported.
     */
    protected loadFallbackBlocks(): void {
        this.blocks = [
            {
                name: 'myoverview',
                visible: true,
            },
            {
                name: 'timeline',
                visible: true,
            },
        ];

        this.hasMainBlocks = CoreBlockDelegate.isBlockSupported('myoverview') || CoreBlockDelegate.isBlockSupported('timeline');
    }

    /**
     * Refresh the dashboard data.
     *
     * @param refresher Refresher.
     */
    refreshDashboard(refresher: HTMLIonRefresherElement): void {
        const promises: Promise<void>[] = [];

        promises.push(CoreCoursesDashboard.invalidateDashboardBlocks());

        // Invalidate the blocks.
        this.blocksComponents?.forEach((blockComponent) => {
            promises.push(blockComponent.invalidate().catch(() => {
                // Ignore errors.
            }));
        });

        Promise.all(promises).finally(() => {
            this.loadContent().finally(() => {
                refresher?.complete();
            });
        });
    }

    /**
     * Go to search courses.
     */
    async openSearch(): Promise<void> {
        CoreNavigator.navigateToSitePath('/courses/list', { params : { mode: 'search' } } as any);
    }

    /**
     * @inheritdoc
     */
    ngOnDestroy(): void {
        this.updateSiteObserver.off();
    }

}
