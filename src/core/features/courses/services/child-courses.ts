export class child_course {
    id?: string;
    name?: string;
    course_image_url?: string;
}


export class child {
    child_email?: string;
    child_id?: string;
    child_image_url?: string;
    child_name?: string;
    child_courses_ids?: string;
    child_courses?: child_course[];
    child_report_link?: string;
    on_child_click?: (child: child) => void;
}
