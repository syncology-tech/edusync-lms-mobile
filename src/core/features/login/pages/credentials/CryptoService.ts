// import necessary modules from CryptoJS
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  private secretKey: string = 'tms_key';

  encrypt(data: string): string {
    // Encrypt the data using AES and the secret key
    const encryptedData = CryptoJS.AES.encrypt(data, this.secretKey).toString();
    return encryptedData;
  }

  decrypt(encryptedData: string): string {
    // Decrypt the data using AES and the secret key
    const decryptedData = CryptoJS.AES.decrypt(encryptedData, this.secretKey).toString(CryptoJS.enc.Utf8);
    return decryptedData;
  }
}
